"""Views and enpoints for polls using rest_framework."""
from django.contrib.auth import authenticate
from rest_framework import generics, status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Poll, Choice
from .serializers import PollSerializer, ChoiceSerializer, VoteSerializer, \
    UserSerializer


class PollViewSet(viewsets.ModelViewSet):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer

    def destroy(self, request, *args, **kwargs):
        """Allows polls to be deleted iff user is the creator."""
        poll = Poll.objects.get(pk=self.kwargs['pk'])

        if not request.user == poll.created_by:
            raise PermissionDenied('You cannot delete this poll.')

        return super().destroy(request, *args, **kwargs)


class ChoiceList(generics.ListCreateAPIView):
    def get_queryset(self):
        return Choice.objects.filter(poll_id=self.kwargs["pk"])

    serializer_class = ChoiceSerializer

    def post(self, request, *args, **kwargs):
        """Allows choice to be created iff user is the creator of the parent
        poll.
        """
        poll = Poll.objects.get(pk=self.kwargs['pk'])

        if not request.user == poll.created_by:
            raise PermissionDenied('You cannot create a choice for this poll.')

        return super().post(request, *args, **kwargs)


class CreateVote(APIView):
    """Defines endpoints for voting"""

    def post(self, request, pk, choice_pk):
        voted_by = request.data.get('voted_by')
        data = {
            'choice': choice_pk,
            'poll': pk,
            'voted_by': voted_by,
        }
        serializer = VoteSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserCreate(generics.CreateAPIView):
    """Defines endpoints for user creation. This class is exempt from the
    global authentication scheme."""
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer


class LoginView(APIView):
    """Defines endpoints for user authentication."""
    permission_classes = ()

    def post(self, request,):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)

        if user:
            return Response({'token': user.auth_token.key})
        else:
            return Response(
                {'error': 'Invalid username or password'},
                status=status.HTTP_400_BAD_REQUEST
            )
