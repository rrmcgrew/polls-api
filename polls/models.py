"""Model classes for the polls app"""
from django.contrib.auth.models import User
from django.db import models


class Poll(models.Model):
    """Representation of a question for which responses are polled."""
    question = models.CharField(max_length=100)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.question


class Choice(models.Model):
    """Represents a response option for a poll."""
    poll = models.ForeignKey(
        Poll, related_name='choices', on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=100)

    def __str__(self):
        return self.choice_text


class Vote(models.Model):
    """Represents the selection of a choice for a poll by a user."""
    choice = models.ForeignKey(
        Choice, related_name='votes', on_delete=models.CASCADE)
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    voted_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('poll', 'voted_by')
