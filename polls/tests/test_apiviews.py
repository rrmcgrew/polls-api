"""Test classes for API views/endpoints."""
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient, APIRequestFactory, APITestCase
from polls import apiviews


class TestPoll(APITestCase):
    """Testing for poll viewset."""

    def setUp(self):
        """Initializes testing variables."""
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.view = apiviews.PollViewSet.as_view({'get': 'list'})
        self.uri = '/polls/'
        self.user = self.setup_user()

    @staticmethod
    def setup_user():
        """Creates a user for authenticated testing."""
        User = get_user_model()

        return User.objects.create_user(
            'test',
            email='test@example.com',
            password='test'
        )

    def test_list(self):
        request = self.factory.get(self.uri)
        request.user = self.user
        response = self.view(request)

        self.assertEqual(
            response.status_code,
            200,
            'Expected response code 200, received {}'.format(
                response.status_code)
        )

    def test_list_via_client(self):
        self.client.login(username='test', password='test')
        response = self.client.get(self.uri)

        self.assertEqual(
            response.status_code,
            200,
            'Expected response code 200, received {}'.format(
                response.status_code)
        )

    def test_create(self):
        self.client.login(username='test', password='test')
        params = {
            'question': 'Who do you think you are?',
            'created_by': 1,
        }
        response = self.client.post(self.uri, params)

        self.assertEqual(
            response.status_code,
            201,
            'Expected response code 201, received {}'.format(
                response.status_code)
        )
