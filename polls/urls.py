"""polls URL Configuration"""
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view
from . import apiviews


schema_view = get_swagger_view(title='Polls API')
router = DefaultRouter()
router.register('polls', apiviews.PollViewSet, base_name='polls')


urlpatterns = [
    path(
        'polls/<int:pk>/choices/',
        apiviews.ChoiceList.as_view(),
        name='choice_list'
    ),
    path(
        'polls/<int:pk>/choices/<int:choice_pk>/vote/',
        apiviews.CreateVote.as_view(),
        name='create_vote'
    ),
    path('swagger-docs/', schema_view, name='swagger'),
]
urlpatterns += router.urls
