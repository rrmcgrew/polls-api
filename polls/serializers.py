"""Serializer classes for polls"""
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models import Poll, Choice, Vote


class VoteSerializer(serializers.ModelSerializer):
    """Serializer class for the Vote model."""
    class Meta:
        model = Vote
        fields = '__all__'


class ChoiceSerializer(serializers.ModelSerializer):
    """Serializer class for the Choice model."""
    votes = VoteSerializer(many=True, required=False)

    class Meta:
        model = Choice
        fields = '__all__'


class PollSerializer(serializers.ModelSerializer):
    """Serializer class for the Poll model."""
    choices = ChoiceSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Poll
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    """Serializer class for User model."""
    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        # ensure the password not returned in responses
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """Creates and returns a user. An authentication token is also created
        for the created user.

        Args:
            validated_data: Valid data required to create a user.

        Returns:
            The created user object.
        """
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        Token.objects.create(user=user)

        return user
