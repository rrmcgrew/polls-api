# Access Control Rules
* A user must be authenticated to access a poll or the list of polls.
* Only an authenticated user can create a poll.
* Only an authenticated user can create a choice.
* Authenticated users can create choices only for polls they created.
* Authenticated users can delete only polls they have created.
* Only an authenticated user can vote. Users can vote on other users' polls.
