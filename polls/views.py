"""View and endpoint definitions for polls."""
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Poll


def polls_list(request):
    """Generates a list of the 20 most recent polls."""
    polls = Poll.objects.all()[:20]
    data = {
        'results': list(polls.values(
            'question',
            'created_by__username',
            'pub_date',
        )),
    }

    return JsonResponse(data)


def polls_detail(request, pk):
    """Generates the details for the poll with the key value passed."""
    poll = get_object_or_404(Poll, pk=pk)
    data = {
        'results': {
            'question': poll.question,
            'created_by': poll.created_by.username,
            'pub_date': poll.pub_date,
        },
    }

    return JsonResponse(data)
